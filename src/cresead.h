#ifndef CREASEAD_H
#define CREASEAD_H

#define SEAD_RAND_MAGIC_1 0x6C078965
#define SEAD_RAND_MAGIC_2 0x6C078967
#define SEAD_RAND_MAGIC_3 0x714ACB41
#define SEAD_RAND_MAGIC_4 0x48077044

#define XOR_LEFT_SHIFT(x, y)  ((x) ^ ((x) << (y)))
#define XOR_RIGHT_SHIFT(x, y) ((x) ^ ((x) >> (y)))

typedef struct sead_random_t
{
    uint32_t context[4];
} sead_random_t;

sead_random_t* sead_random_init(uint32_t s);
sead_random_t* sead_random_multiple_seed_init(uint32_t s1, uint32_t s2,
                                              uint32_t s3, uint32_t s4);
void sead_random_set_seed(sead_random_t* r, uint32_t s);
uint32_t sead_random_get_u32(sead_random_t* r);
uint64_t sead_random_get_u64(sead_random_t* r);
void sead_random_destroy(sead_random_t** _r);

#endif
