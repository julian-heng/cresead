#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "cresead.h"


sead_random_t*
sead_random_init(uint32_t s)
{
    sead_random_t* r;
    r = calloc(1, sizeof(sead_random_t));
    if (r == NULL)
        goto mem_error;

    sead_random_set_seed(r, s);
    return r;

mem_error:
    fprintf(stderr, "cresead: Out of memory\n");
    return NULL;
}

sead_random_t*
sead_random_multiple_seed_init(uint32_t s1, uint32_t s2,
                               uint32_t s3, uint32_t s4)
{
    sead_random_t* r;
    r = calloc(1, sizeof(sead_random_t));
    if (r == NULL)
        goto mem_error;

    if ((s1 | s2 | s3 | s4) == 0)
    {
        s1 = 1;
        s2 = SEAD_RAND_MAGIC_2;
        s3 = SEAD_RAND_MAGIC_3;
        s4 = SEAD_RAND_MAGIC_4;
    }

    r->context[0] = s1;
    r->context[1] = s2;
    r->context[2] = s3;
    r->context[3] = s4;

    return r;

mem_error:
    fprintf(stderr, "cresead: Out of memory\n");
    return NULL;
}


void
sead_random_set_seed(sead_random_t* r, uint32_t s)
{
    r->context[0] = SEAD_RAND_MAGIC_1 * XOR_RIGHT_SHIFT(s, 30) + 1;
    r->context[1] = SEAD_RAND_MAGIC_1 * XOR_RIGHT_SHIFT(r->context[0], 30) + 2;
    r->context[2] = SEAD_RAND_MAGIC_1 * XOR_RIGHT_SHIFT(r->context[1], 30) + 3;
    r->context[3] = SEAD_RAND_MAGIC_1 * XOR_RIGHT_SHIFT(r->context[2], 30) + 4;
}


uint32_t
sead_random_get_u32(sead_random_t* r)
{
    uint32_t n;

    n = XOR_LEFT_SHIFT(r->context[0], 11);
    r->context[0] = r->context[1];
    r->context[1] = r->context[2];
    r->context[2] = r->context[3];
    r->context[3] = XOR_RIGHT_SHIFT(n, 8) ^ XOR_RIGHT_SHIFT(r->context[3], 19);

    return r->context[3];
}

uint64_t
sead_random_get_u64(sead_random_t* r)
{
    uint32_t n1 = XOR_LEFT_SHIFT(r->context[0], 11);
    uint32_t n2 = r->context[1];
    uint32_t n3 = XOR_RIGHT_SHIFT(n1, 8) ^ r->context[3];

    r->context[0] = r->context[2];
    r->context[1] = r->context[3];
    r->context[2] = n3 ^ (r->context[3] >> 19);
    r->context[3] = XOR_LEFT_SHIFT(n2, 11) ^ (XOR_LEFT_SHIFT(n2, 11) >> 8) ^ r->context[2] ^ (n3 >> 19);

    return ((uint64_t)r->context[2] << 32) | r->context[3];
}


void
sead_random_destroy(sead_random_t** _r)
{
    if (*_r != NULL)
    {
        free(*_r);
        *_r = NULL;
    }
}
