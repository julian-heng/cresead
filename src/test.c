#define _POSIX_C_SOURCE 199309L

#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "cresead.h"

uint32_t get_tick(void);

int
main(void)
{
    sead_random_t* rand = sead_random_init(get_tick());

    for (int i = 0; i < 100; i++)
        printf("%u\n", sead_random_get_u32(rand));

    sead_random_destroy(&rand);

    return 0;
}


uint32_t
get_tick(void)
{
    struct timespec ts;
    uint32_t tick = 0U;
    clock_gettime(CLOCK_REALTIME, &ts);
    tick = ts.tv_nsec / 1000000;
    tick += ts.tv_sec * 1000;
    return tick;
}
